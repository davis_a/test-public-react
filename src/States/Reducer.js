
import { InitialState} from './Storage'

const reducer = (state, action) => {
    switch(action.type){
        case 'get_companies':
        {
            return {
                ...state,
                lstCompanies: action.lstCompanies,
                Loading: action.Loading,
                dataExport: action.dataExport
            };
        }
        case 'get_students':
        {
            return {
                ...state,
                lstStudents: action.lstStudents,
                Loading: action.Loading,
                dataExport: action.dataExport
            };
        }
        case 'get_offers':
        {
            return {
                ...state,
                lstOffers: action.lstOffers,
                Loading: action.Loading
            };
        }
        case 'get_services':
        {
            return {
                ...state,
                lstServices: action.lstServices,
                Loading: action.Loading
            }
        }
        case 'get_postulates':
        {
            return {
                ...state,
                lstPostulates: action.lstPostulates,
                Loading: action.Loading
            }
        }
        case 'delete_Company':
        {
            return {
                ...state,
                lstCompanies: action.lstCompanies.filter(x => x.CompanyId !== action.CompanyId)
            }
        }
        case 'delete_Offer':
        {
            return {
                ...state,
                lstOffers: action.lstOffers.filter(x => x.JobOfferId !== action.JobOfferId)
            }
        }
        case 'get_typesidentification':
        {
            return {
                ...state,
                lstTypesIdentification: action.lstTypesIdentification,
                Loading: action.Loading
            };
        }
        case 'get_location':
        {
            return {
                ...state,
                lstDepartments: action.lstDepartments,
                lstCities: action.lstCities,
                Loading: action.Loading
            };
        }
        case 'login':
        {
            return {
                ...state,
                Token: action.Token,
                SelectedMenu: action.SelectedMenu,
                Loading: action.Loading,
                DateToken: new Date()
            };
        }
        case 'logout':
        {
            InitialState.PositionTab = action.PositionTab;
            return InitialState;
        }
        case 'position_tab':
        {
            return {
                ...state,
                PositionTab: action.PositionTab,
            };
        }
        case 'selected_menu':
        {
            return {
                ...state,
                SelectedMenu: action.SelectedMenu,
            };
        }
        case 'load_element':
        {
            return {
                ...state,
                Element: action.Element,
            };
        }
        case 'change_loading':
        {
            return {
                ...state,
                Loading: action.Loading
            };
        }
        default: {
            return {...state};
        }
    }
};

export {reducer};