import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {loadState, saveState} from './Storage'
import { reducer } from './Reducer';

const StateLoad = loadState();

const logger = store => next => action => {
    console.log('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    return result;
}

const store = createStore(reducer, StateLoad , applyMiddleware(thunk));

store.subscribe( function () {
    saveState(store.getState());
})

export default store;