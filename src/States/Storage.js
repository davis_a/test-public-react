
const InitialState = { 
  lstMenu: [
    {'Name':'Empresas','Rol':'Admin', 'actionMenu':'Company'},
    {'Name':'Estudiantes','Rol':'Admin', 'actionMenu':'Student'},
    {'Name':'Salir','Rol':'Admin', 'actionMenu':'Login'},
  ], 
  Token: "", DateToken: new Date(), Loading: false, PositionTab: "top", lstCompanies: [],
  Element: null,  SelectedMenu: "Login",  lstTypesIdentification: [],  lstDepartments: [],
  lstCities: [],  lstOffers: [],  lstServices: [],  lstStudents: [],  dataExport: [],
  lstPostulates: []
}

const loadState = () => {
  try {
    const serializedData = localStorage.getItem('state')
    if (serializedData === null){
      return InitialState // Si no existe el state en el local storage devolvemos undefined para que cargue el state inicial que hayamos definido
    }
    return JSON.parse(serializedData) // Si encontramos con exito nuestro storage lo devolvemos.
  } catch (error) {
    return InitialState // Si ocurre algun error, devuelvo undefined para cargar el state inicial.
  }
}

const saveState = (state) => {
  try {
    let serializedData = JSON.stringify(state)
    localStorage.setItem('state', serializedData)
  } catch (error) {
	// Acá podemos capturar o crear cualquier log que deseemos en caso de que falle el salvado en el storage.    
  }
}

export {loadState, saveState, InitialState};