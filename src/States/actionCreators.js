import { get, post } from '../Services/Services';
import { Modal } from 'antd';

const host = process.env.NODE_ENV === 'production' ? 'http://workuniversity.co' : 'http://workuniversityqa.azurewebsites.net';

const postLogin = (User, Password) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            dispatch({
                type: "login",
                Token: Data.access_token,
                SelectedMenu: "Company",
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.error_description,
                okText: 'Cerrar'
            });
        };
        let data = "grant_type=password&username=" + User + "&password=" + Password;
        return post("/Token", Success, Errors, data);
    };
}

const postSaveCompany = (Company, lstCompanies, Token) => {
    return dispatch => {
        const Success = (Response) => {
            let index = lstCompanies.findIndex(x => x.CompanyId === Company.CompanyId);
            lstCompanies[index] = Company;
            dispatch({
                type: "get_companies",
                lstCompanies: lstCompanies,
                Loading: false
            });
            Modal.success({
                title: 'Notificaciones',
                content: Response.data.Message,
                okText: 'Cerrar'
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.error_description,
                okText: 'Cerrar'
            });
        };
        let headers = {Authorization: "Bearer "+ Token};
        return post("/Api/AccountCompany/UpdateCompany", Success, Errors, Company, headers);
    };
}

const postSaveOffer = (JobOffer, lstOffers, Token) => {
    return dispatch => {
        const Success = (Response) => {
            let index = lstOffers.findIndex(x => x.JobOfferId === JobOffer.JobOfferId);
            lstOffers[index] = JobOffer;
            dispatch({
                type: "get_offers",
                lstOffers: lstOffers,
                Loading: false
            });
            Modal.success({
                title: 'Notificaciones',
                content: Response.data.Message,
                okText: 'Cerrar'
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.error_description,
                okText: 'Cerrar'
            });
        };
        let headers = {Authorization: "Bearer "+ Token};
        return post("/Api/JobOffers/UpdateobOffer", Success, Errors, JobOffer, headers);
    };
}

const deleteCompany = (CompanyId, lstCompanies) => {
    return {
        type: "delete_Company",
        CompanyId,
        lstCompanies
    };
}

const deleteOffer = (JobOfferId, lstOffers) => {
    return {
        type: "delete_Offer",
        JobOfferId,
        lstOffers
    };
}

const getCompanies = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            let array = [];
            Data.ModelData.map((item, index) => {
                let obj = {
                    "NameCompany": item.Name,
                    "BusinessName": item.BusinessName,
                    "Identification": item.Identification,
                    "Address": item.Address,
                    "Phone": item.Phone,
                    "Ext": item.Ext,
                    "Email": item.Email,
                    "Department": item.Department.Name,
                    "City": item.City.Name,
                    "NamePerson": item.objPerson.FirstName + " " + item.objPerson.LastName,
                };
                array.push(obj);
            });
            dispatch({
                type: "get_companies",
                lstCompanies: Data.ModelData,
                Loading: false,
                dataExport: array
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/Generic/GetCompanies", Success, Errors, headers);
    };
}

const getPostulates = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            if(!Response.data.Status){
                Modal.error({
                    title: 'Notificaciones',
                    content: Response.Message,
                    okText: 'Cerrar'
                });
                return;
            }
            dispatch({
                type: "get_postulates",
                lstPostulates: Response.data.ModelData,
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };
        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/JobOffers/getStudentsByOffers", Success, Errors, headers);
    };
}

const getStudents = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            let array = [];
            Data.ModelData.map((item, index) => {
                let Languages = "";
                item.StudentByLanguages.map((language, index2) => {
                    Languages += language.Language.Name + ": " + language.Level;
                    Languages += (index2 + 1) < item.StudentByLanguages.length ? ", " : "";
                });
                let Universities = "";
                item.UniversityByStudents.map((university, index2) => {
                    Universities += university.University.Name + ": " + university.Program.Name + " semestre: " + university.Semester;
                    Universities += (index2 + 1) < item.UniversityByStudents.length ? ", " : "";
                });
                let Abilities = "";
                item.StudentByAbility.map((ability, index2) => {
                    Abilities += ability.Ability.Name;
                    Abilities += (index2 + 1) < item.StudentByAbility.length ? ", " : "";
                });
                let obj = {
                    "FirstName": item.Person.FirstName,
                    "LastName": item.Person.LastName,
                    "Age": item.Person.Age,
                    "Gender": item.Person.Gender,
                    "CellPhone": item.Person.CellPhone,
                    "Phone": item.Person.Phone,
                    "Address": item.Person.Address,
                    "Email": item.Person.Email,
                    "Department": item.Person.Department.Name,
                    "City": item.Person.City.Name,
                    "Languages": Languages,
                    "Universities": Universities,
                    "Abilities": Abilities,
                    "Experience": item.WorkExperience,
                    "Curriculum": item.CV !== null ? host + item.CV : ''
                };
                array.push(obj);
            });
            dispatch({
                type: "get_students",
                lstStudents: Data.ModelData,
                Loading: false,
                dataExport: array,
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/Generic/GetStudents", Success, Errors, headers);
    };
}

const getOffers = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            dispatch({
                type: "get_offers",
                lstOffers: Data.ModelData,
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/JobOffers/GetAllJobOffers", Success, Errors, headers);
    };
}

const getServices = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            dispatch({
                type: "get_services",
                lstServices: Data.ModelData,
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/Services/GetServices", Success, Errors, headers);
    };
}

const ChangeStatusOffer = (Token, lstOffers, OfferId, Status) => {
    return dispatch => {
        const Success = (Response) => {
            let objIndex = lstOffers.findIndex((obj => obj.JobOfferId === OfferId));
            lstOffers[objIndex].Status = Status;
            dispatch({
                type: "get_offers",
                lstOffers: lstOffers,
                Loading: false
            });
            Modal.success({
                title: 'Notificaciones',
                content: Response.data.Message,
                okText: 'Cerrar'
            });     
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/JobOffers/ChangeStatusOffer?OfferId="+OfferId+"&Status="+Status, Success, Errors, headers);
    };
}

const getTypesIdentification = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            dispatch({
                type: "get_typesidentification",
                lstTypesIdentification: Data.Model,
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/Generic/TypesIdentification", Success, Errors, headers);
    };
}

const getLocation = (Token) => {
    return dispatch => {
        const Success = (Response) => {
            let Data = Response.data;
            dispatch({
                type: "get_location",
                lstDepartments: Data.ModelDepartments,
                lstCities: Data.ModelCities,
                Loading: false
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.Message,
                okText: 'Cerrar'
            });
        };

        let headers = {Authorization: "Bearer "+ Token};

        return get("/Api/Generic/Location", Success, Errors, headers);
    };
}

const changePosition = (PositionTab) => {
    return {
        type: "position_tab",
        PositionTab
    };
};

const selectedMenu = (SelectedMenu, Token, PositionTab) => {
    if(SelectedMenu === "Login"){
        return dispatch => {
            const Success = (Response) => {
                dispatch({
                    type: "logout",
                    PositionTab
                });
            };
            const Errors = (error) => {
                Modal.error({
                    title: 'Notificaciones',
                    content: error.response.data.Message,
                    okText: 'Cerrar'
                });
            };
    
    
            return post("/Api/Account/Logout", Success, Errors, Token);
        };
    }
    else
    {
        return {
            type: "selected_menu",
            SelectedMenu
        };
    }
    
};

const LoadElement = (Element) => {
    return {
        type: "load_element",
        Element
    };
};

const changeLoading = (Loading) => {
    return {
        type: "change_loading",
        Loading
    };
};

const SaveInvoice = (formData, CompanyId, NumInvoice, Token) => {
    return dispatch => {
        const Success = (Response) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.success({
                title: 'Notificaciones',
                content: Response.data.Message,
                okText: 'Cerrar'
            });
        };
        const Errors = (error) => {
            dispatch({
                type: "change_loading",
                Loading: false
            });
            Modal.error({
                title: 'Notificaciones',
                content: error.response.data.error_description,
                okText: 'Cerrar'
            });
        };
        let headers = {Authorization: "Bearer "+ Token, 'Content-Type': 'multipart/form-data'};
        return post("/Api/Invoices/CreateInvoice?CompanyId=" + CompanyId + "&NumInvoice="+ NumInvoice + "&NameFile=Invoice" , Success, Errors, formData, headers);
    };
};

export { host, postLogin, changeLoading, changePosition, getCompanies, selectedMenu, LoadElement, getTypesIdentification,
     getLocation, postSaveCompany, deleteCompany, getOffers, ChangeStatusOffer, deleteOffer, postSaveOffer, getServices,
     getStudents, getPostulates, SaveInvoice };