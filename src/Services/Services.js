import axios from 'axios';

const host = process.env.NODE_ENV === 'production' ? 'http://workuniversity.co' : 'http://workuniversityqa.azurewebsites.net';

const get = (url, Success, Errors, headers={}) => {
    axios.get(host + url, {headers})
        .then((response) => {
            Success(response);
        })
        .catch(function (error) {
            Errors(error);
        });
};

const post = (url, Success, Errors, data, headers={}) => {
    axios.post(host + url, data,{headers}
        ).then((response) => {
            Success(response);
        })
        .catch((error) => {
            Errors(error);
        });
};

export { get, post };