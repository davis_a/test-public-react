import React, { Component } from 'react';
import { Select, Form, Input, Button, Modal } from 'antd';
import styled from 'styled-components';

const Option = Select.Option;
const FormItem = Form.Item;

const SelectPer = styled(Select)`
    width: 300px;
`;

class Formulario extends Component {
    constructor() {
        super();
        this.state = {
            languages: [
                {
                    code: "1",
                    name: "Ingles"
                },
                {
                    code: "2",
                    name: "Español"
                },
                {
                    code: "3",
                    name: "Frances"
                },
            ],
            Loading: false
        };
    }

    handleSubmit = (e) => {
        this.setState({
            Loading: true
        });
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let ObjLanguage = {
                    code: values.code,
                    name: values.name
                };
                let lstLanguages = this.state.languages;
                lstLanguages.push(ObjLanguage);
                this.setState({
                    languages: lstLanguages
                });
                Modal.success({
                    title: 'Notificaciones',
                    content: "Se ha cambiado con exito",
                    okText: 'Cerrar'
                });
            }
            this.setState({
                Loading: false
            });
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="Formulario">
                <SelectPer
                    placeholder="-- seleccione un idioma --">
                    {this.state.languages.map((item, index) => <Option key={index} value={item.code}>{item.name}</Option>)}
                </SelectPer>
                <hr></hr>
                <Form onSubmit={this.handleSubmit} className="test-form">
                    <FormItem>
                        {getFieldDecorator('code', {
                            rules: [{ required: true, message: 'Por favor ingrese un código!' }],
                        })(
                            <Input placeholder="Código" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Por favor ingrese un nombre' }],
                        })(
                            <Input placeholder="nombre" />
                        )}
                    </FormItem>
                    <FormItem>
                        <Button loading={this.state.Loading} type="primary" htmlType="submit" className="test-form-button">Guardar</Button>
                    </FormItem>
                </Form>
            </div>
        );
    };
}

const Form1 = Form.create()(Formulario);

export default Form1;