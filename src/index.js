import React from 'react';
import ReactDOM from 'react-dom';
/*import './Styles/index.css';*/
/*import Router from './Components/Router';*/
import registerServiceWorker from './registerServiceWorker';
import 'antd/dist/antd.css';
import store from './States/store'
import { Provider } from 'react-redux';
import Home from './Views/Home';
import Formulario from './Views/Formulario';

ReactDOM.render(
    <Formulario />, document.getElementById('root'));
registerServiceWorker();

/*let tituloH1 = document.createElement("h1");
tituloH1.textContent = "Hola mundo."
tituloH1.style.color = "#000";
let span = document.createElement("span");
tituloH1.appendChild(span);
document.render(tituloH1);

<h1>
    hola mundo
   <span></span> 
</h1>


let titulo2 = ReactDOM.createElement("h1");
titulo2.textContent = "Hola mundo";
let span2 = document.createElement("span");
titulo2.appendChild(span, span2);
ReactDOM.render(titulo2);

<h1>
    hola mundo
   <span></span> 
   <span></span> 
</h1>*/
